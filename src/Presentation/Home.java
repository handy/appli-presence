package Presentation;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

public class Home {
    private static JMenuBar menuBar;
    private JPanel RootPanel;



    private Color Deep_Koamaru = new Color(0x3e3b75);
    private Color Pastel_Green = new Color(0x6edd58);
    private Color Glitter = new Color(0xe8eaf2);
    private Color Dark_Jungle_Green = new Color(0x1f271b);
    private Color Space_Cadet = new Color(0x29274f);


    public void init() {
        RootPanel.setBackground(Space_Cadet);



        menuBar = new JMenuBar();
        menuBar.setForeground(Glitter);
        menuBar.setBackground(Deep_Koamaru);
        menuBar.setBorder(BorderFactory.createMatteBorder(0,0,0,0,Color.black));
        JMenu menu = new JMenu("Ajouter");
        menu.setForeground(Glitter);
        menu.setBackground(Deep_Koamaru);
        menuBar.add(menu);
        menu.getPopupMenu().setBorder(null);

        JMenuItem menuItemForge = new JMenuItem("Forge");
        menuItemForge.setForeground(Glitter);
        menuItemForge.setBackground(Deep_Koamaru);
        menuItemForge.setBorder(BorderFactory.createMatteBorder(0,0,0,0,Color.black));
        menu.add(menuItemForge);
        JMenuItem menuItemFormation = new JMenuItem("Formation");
        menuItemFormation.setForeground(Glitter);
        menuItemFormation.setBackground(Deep_Koamaru);
        menuItemFormation.setBorder(BorderFactory.createMatteBorder(0,0,0,0,Color.black));
        menu.add(menuItemFormation);



    }

    public Home() {
        init();
    }

    public static void main(String[] args) {

        JFrame frame = new JFrame("Home");
        frame.setContentPane(new Home().RootPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setJMenuBar(menuBar);
        frame.setVisible(true);
    }
}

package DTO;

import java.io.Serializable;
import java.util.Date;

public class PromotionDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String nom;
    private Date dateDebut;
    private Date dateFin;
    private int id_formation;

    public String getNom() {
        return nom;
    }

    public Date getDateDebut() {
        return dateDebut;
    }

    public Date getDateFin() {
        return dateFin;
    }

    public int getId_formation() {
        return id_formation;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;
    }

    public void setDateFin(Date dateFin) {
        this.dateFin = dateFin;
    }

    public void setId_formation(int id_formation) {
        this.id_formation = id_formation;
    }

    public PromotionDTO(String nom, Date dateDebut, Date dateFin, int id_formation) {
        this.nom = nom;
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
        this.id_formation = id_formation;
    }

    @Override
    public String toString() {
        return  nom + "\n" +
                dateDebut + " " +dateFin + "\n" +
                id_formation;
    }
}

package DTO;

import java.io.Serializable;
import java.util.Date;

public class PresenceDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Date date;
    private String sujet;
    private int formateur;
    private int promotion;

    public Date getDate() {
        return date;
    }

    public String getSujet() {
        return sujet;
    }

    public int getFormateur() {
        return formateur;
    }

    public int getPromotion() {
        return promotion;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setSujet(String sujet) {
        this.sujet = sujet;
    }

    public void setFormateur(int formateur) {
        this.formateur = formateur;
    }

    public void setPromotion(int promotion) {
        this.promotion = promotion;
    }

    public PresenceDTO(Date date, String sujet, int formateur, int promotion) {
        this.date = date;
        this.sujet = sujet;
        this.formateur = formateur;
        this.promotion = promotion;
    }

    @Override
    public String toString() {
        return  date + "\n"+
                sujet + "\n" +
                formateur + "\n" +
                promotion;
    }
}

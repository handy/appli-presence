package DTO;

import Service.RoleEnum;

import java.io.Serializable;

public class PersonnelDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String nom;
    private String prenom;
    private RoleEnum role;
    private int manager;
    private int forge;

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public RoleEnum getRole() {
        return role;
    }

    public int getManager() {
        return manager;
    }

    public int getForge() {
        return forge;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public void setRole(RoleEnum role) {
        this.role = role;
    }

    public void setManager(int manager) {
        this.manager = manager;
    }

    public void setForge(int forge) {
        this.forge = forge;
    }

    public PersonnelDTO(String nom, String prenom, int manager, RoleEnum role) {
        this.nom = nom;
        this.prenom = prenom;
        this.role = role;
        this.manager = manager;
    }

    public PersonnelDTO(String nom, String prenom, RoleEnum role, int forge) {
        this.nom = nom;
        this.prenom = prenom;
        this.role = role;
        this.forge = forge;
    }

    @Override
    public String toString() {
        return nom + " " + prenom + " " + role + "\n" +
                manager + forge;
    }
}

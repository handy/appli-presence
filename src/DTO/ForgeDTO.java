package DTO;

import java.io.Serializable;

public class ForgeDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String adresse;
    private String nom;

    public String getAdresse() {
        return adresse;
    }

    public String getNom() {
        return nom;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public ForgeDTO(String adresse, String nom) {
        this.adresse = adresse;
        this.nom = nom;
    }

    @Override
    public String toString() {
        return nom + " " + adresse;
    }
}

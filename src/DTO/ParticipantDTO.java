package DTO;

import java.io.Serializable;

public class ParticipantDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String nom;
    private String prenom;
    private String motif;
    private int promotion;

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public String getMotif() {
        return motif;
    }

    public int getPromotion() {
        return promotion;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public void setMotif(String motif) {
        this.motif = motif;
    }

    public void setPromotion(int promotion) {
        this.promotion = promotion;
    }

    public ParticipantDTO(String nom, String prenom, String motif, int promotion) {
        this.nom = nom;
        this.prenom = prenom;
        this.motif = motif;
        this.promotion = promotion;
    }

    @Override
    public String toString() {
        return  nom + prenom + '\n' +
                motif + '\n' + promotion;
    }
}

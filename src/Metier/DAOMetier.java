package Metier;

import DTO.ForgeDTO;
import DTO.FormationDTO;
import DTO.ParticipantDTO;
import DTO.PersonnelDTO;
import DTO.PresenceDTO;
import DTO.PromotionDTO;
import Service.ConnectionAppli;
import Service.RoleEnum;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DAOMetier implements IDAOMetier {

    private final static String SQLFindAllForge =
            "SELECT id, nom, adresse " +
                    "FROM FORGE ";

    private final static String SQLInsertForge =
            "INSERT INTO FORGE VALUES (?, ?)";

    @Override
    public Boolean creerUneForge(String adresse, String nom) {
        Boolean resultat = false;
        try {
            Connection conn = ConnectionAppli.getInstance();
            PreparedStatement pstm = conn.prepareStatement(SQLInsertForge);
            pstm.setString(1, nom);
            pstm.setString(2, adresse);
            ResultSet rs = pstm.executeQuery();
            if (rs.next()) {
                resultat = true;
            }
        } catch (SQLException e) {
            //TODO Auto-generated catch block
            e.printStackTrace();
        }
        return resultat;

    }

    @Override
    public void creerUneFormation(String nom) {

    }

    @Override
    public void creerUnePromotion(String nom, Date dateDebut, Date dateFin, int id_formation) {

    }

    @Override
    public void creerUnMembreDuPersonnel(String nom, String prenom, int manager, RoleEnum role) {

    }

    @Override
    public void creerUnChefDeProjet(String nom, String prenom, int forge) {

    }

    @Override
    public void creerUnParticipant(String nom, String prenom, int promotion) {

    }

    @Override
    public void creerUnePresence(Date date, String sujet, int formateur, int promotion) {

    }

    @Override
    public void modifierUneFormation(int id, String nom) {

    }

    @Override
    public void modifierUnePromotion(int id, String nom, Date dateDebut, Date dateFin, int id_formation) {

    }

    @Override
    public void modifierUnMembreDuPersonnel(int id, String nom, String prenom, int manager, RoleEnum role) {

    }

    @Override
    public void modifierUnChefDeProjet(int id, String nom, String prenom, RoleEnum role, int forge) {

    }

    @Override
    public void modifierUnParticipant(int id, String nom, String prenom, int promotion) {

    }

    @Override
    public void modifierUnePresence(int id, Date date, String sujet, int formateur, int promotion) {

    }

    @Override
    public void associerFormationForge(int idForge, int idFormation) {

    }

    @Override
    public void associerPersonnelFormation(int idPersonnel, int idFormation) {

    }

    @Override
    public void associerPromotionParticipant(int idPromotion, int idParticipant) {

    }

    @Override
    public void associerPresenceParticipant(int idPresence, int idParticipant) {

    }

    @Override
    public List<ForgeDTO> esnForges() {
        List<ForgeDTO> liste = new ArrayList<>();
        try {
            Statement instr =
                    ConnectionAppli.getInstance().createStatement();
            ResultSet rs = instr.executeQuery(SQLFindAllForge);

            while (rs.next()) {
                String nom = rs.getString(2);
                String adresse = rs.getString(3);

                liste.add(new ForgeDTO(nom, adresse));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return liste;
    }

    @Override
    public List<FormationDTO> ensFormations() {
        return null;
    }

    @Override
    public List<ParticipantDTO> ensParticipants() {
        return null;
    }

    @Override
    public List<PersonnelDTO> ensPersonnels() {
        return null;
    }

    @Override
    public List<PresenceDTO> ensPresences() {
        return null;
    }

    @Override
    public List<PromotionDTO> ensPromotions() {
        return null;
    }

    @Override
    public List<ParticipantDTO> ensParticipantPromotion(int idPromotion) {
        return null;
    }

    @Override
    public List<PersonnelDTO> ensPersonnelPromotion(int idPromotion) {
        return null;
    }

    @Override
    public List<PresenceDTO> ensPresencePromotion(int idPromotion) {
        return null;
    }
}

package Metier;

import DTO.ForgeDTO;
import DTO.FormationDTO;
import DTO.ParticipantDTO;
import DTO.PersonnelDTO;
import DTO.PresenceDTO;
import DTO.PromotionDTO;
import Service.RoleEnum;

import java.util.Date;
import java.util.List;

public interface IDAOMetier {

    Boolean creerUneForge(String adresse, String nom);

    void creerUneFormation(String nom);

    void creerUnePromotion(String nom, Date dateDebut, Date dateFin, int id_formation);

    void creerUnMembreDuPersonnel(String nom, String prenom, int manager, RoleEnum role);

    void creerUnChefDeProjet(String nom, String prenom, int forge);

    void creerUnParticipant(String nom, String prenom, int promotion);

    void creerUnePresence(Date date, String sujet, int formateur, int promotion);

    void modifierUneFormation(int id, String nom);

    void modifierUnePromotion(int id, String nom, Date dateDebut, Date dateFin, int id_formation);

    void modifierUnMembreDuPersonnel(int id, String nom, String prenom, int manager, RoleEnum role);

    void modifierUnChefDeProjet(int id, String nom, String prenom, RoleEnum role, int forge);

    void modifierUnParticipant(int id, String nom, String prenom, int promotion);

    void modifierUnePresence(int id, Date date, String sujet, int formateur, int promotion);

    void associerFormationForge(int idForge, int idFormation);

    void associerPersonnelFormation(int idPersonnel, int idFormation);

    void associerPromotionParticipant(int idPromotion, int idParticipant);

    void associerPresenceParticipant(int idPresence, int idParticipant);

    List<ForgeDTO> esnForges();
    List<FormationDTO> ensFormations();
    List<ParticipantDTO> ensParticipants();
    List<PersonnelDTO> ensPersonnels();
    List<PresenceDTO> ensPresences();
    List<PromotionDTO> ensPromotions();

    List<ParticipantDTO> ensParticipantPromotion(int idPromotion);
    List<PersonnelDTO> ensPersonnelPromotion(int idPromotion);
    List<PresenceDTO> ensPresencePromotion(int idPromotion);


}
